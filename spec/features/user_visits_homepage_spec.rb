require 'rails_helper'
feature 'User visits Home Page' do
  scenario 'successfully' do
    visit root_path
    expect(page).to have_content 'Bem vindo ao Opinions App'
  end
end
