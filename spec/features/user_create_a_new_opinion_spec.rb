require 'rails_helper'
feature 'User create a new opinion' do
  scenario 'successfully' do
    opinion = 'Rails is awesome!'
    visit new_opinion_path
    fill_in 'Body', with: opinion
    click_on 'Create Opinion'
    expect(page).to have_content opinion
  end
end
