feature "User sees opinions" do
  scenario "successfully" do
    opinions = ["Rails is awesome!", "That's Rails, bitch!", "Ruby rules!"]
    opinions.each { |o| Opinion.create body: o}
    visit opinions_path
    opinions.each { |o| expect(page).to have_content o}
  end
end
