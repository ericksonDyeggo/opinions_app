class OpinionsController < ApplicationController
  def new
    @opinion = Opinion.new
  end
  def create
    @opinion = Opinion.create get_opinion_params
    redirect_to @opinion
  end
  def show
    @opinion = get_opinion_by_id
  end
  def index
    @opinions = Opinion.all
  end
  private
  def get_opinion_by_id
    Opinion.find params[:id]
  end
  def get_opinion_params
    { body: params[:opinion][:body] }
  end
end
